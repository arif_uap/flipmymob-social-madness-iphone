//
//  main.m
//  FlipMobiPhone
//
//  Created by Md Arifuzzaman on 1/14/14.
//  Copyright SELISE 2014. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"FlipMobiPhoneAppDelegate");
    [pool release];
    return retVal;
}
