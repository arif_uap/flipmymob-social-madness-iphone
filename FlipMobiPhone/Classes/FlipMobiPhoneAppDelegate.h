/**
 *  FlipMobiPhoneAppDelegate.h
 *  FlipMobiPhone
 *
 *  Created by Md Arifuzzaman on 1/14/14.
 *  Copyright SELISE 2014. All rights reserved.
 */

#import <UIKit/UIKit.h>
#import "CC3DeviceCameraOverlayUIViewController.h"

@interface FlipMobiPhoneAppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow* _window;
	CC3DeviceCameraOverlayUIViewController* _viewController;
}
@end
